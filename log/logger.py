import logging

class Logger:
    def __init__(self, loglevel: int = logging.INFO) -> None:
        self.loglevel = loglevel
        logging.basicConfig(level = logging.INFO)
        self._logger = logging.getLogger('root')
        self._logger.info('logger ready')

    def __getattr__(self, name):
        if hasattr(self._logger, name):
            return getattr(self._logger, name)
        else:
            return getattr(self, name)

    @property
    def loglevel(self) -> int: return self._loglevel

    @loglevel.setter
    def loglevel(self, value: int) -> None: self._loglevel = value

if __name__ == '__main__':
    logger: Logger = Logger()
    logger.info(f'using logger, loglevel: {logger.loglevel}')
    logger.loglevel = logging.WARN
    logger.info(f'using logger, loglevel: {logger.loglevel}')
