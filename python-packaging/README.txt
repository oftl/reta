### venv

$ python -m venv project
$ source project/bin/activate

### install package

pip install 'SomeProject==1.4'  # >= ~=
pip install
    --upgrade
    --user
    --requirement requirements.txt
    --editable git+https://...
    [--editable] <local src path>
    --index-url https://local/repo SomeProject

### create package

# requirements
$ python3 -m pip install --user --upgrade setuptools wheel
$ python3 -m pip install --user --upgrade twine

# build distribution
$ cd funky/
$ python3 setup.py sdist bdist_wheel

# upload distribution
$ twine upload --repository-url https://test.pypi.org/legacy/ dist/*

# see new package "funky" at https://test.pypi.org/oftl/funky

# test it
docker build --file Dockerfile.funky --tag funky: .
docker run --rm -it funky
