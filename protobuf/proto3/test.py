"""
abc
"""

import datetime

import pytest

import types_pb2


def test01():
    message = "shit happens"
    code = 23
    details = types_pb2.ErrorDetails()
    details.message = message
    details.code = code

    es = types_pb2.ErrorStatus()
    # pack ...
    es.details.Pack(details)
    unpacked = types_pb2.ErrorDetails()
    # ... and unpack details
    es.details.Unpack(unpacked)

    assert code == unpacked.code
    assert message == unpacked.message


def test_time():
    incident = types_pb2.Incident()
    epoch = "1970-01-01T00:00:00Z"
    incident.start_at.FromJsonString(epoch)

    assert incident.start_at.ToJsonString() == epoch

    # get `now` and a clumpsy test
    incident.start_at.GetCurrentTime()
    assert incident.start_at.ToJsonString() != epoch

    dt = datetime.datetime(1968, 1, 1)
    incident.start_at.FromDatetime(dt)
    assert incident.start_at.ToJsonString() == "1968-01-01T00:00:00Z"

    incident.duration.FromNanoseconds(1999999999)
    td = incident.duration.ToTimedelta()
    assert td.seconds == 1
    assert td.microseconds == 999999


def test_struct():
    stuff01 = types_pb2.Stuff01()
    stuff01.structMsg["key1"] = "KEY1"
    assert stuff01.structMsg["key1"] == "KEY1"

    stuff01.structMsg.get_or_create_struct("key2")["foo"] = "KEY2"
    assert stuff01.structMsg["key2"]["foo"] == "KEY2"

    stuff02 = types_pb2.Stuff01()
    stuff02.structMsg.get_or_create_list("key5")
    stuff02.listMsg.get_or_create_list("key5")

    lst = stuff02.listMsg.get_or_create_list("key5")


def test_struct02():
    stuff01 = types_pb2.Stuff01()
    stuff01.structMsg["a"] = "A"
    stuff01.structMsg["b"] = "B"
    stuff01.structMsg["c"] = "C"

    assert len(stuff01.structMsg) == 3

def test_struct02():
    stuff01 = types_pb2.Stuff01()
    stuff01.structMsg["a"] = "A"
    stuff01.structMsg["b"] = "B"
    stuff01.structMsg["c"] = "C"

    assert len(stuff01.structMsg) == 3
