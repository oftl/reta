import pytest

import addressbook_pb2

@pytest.fixture
def person():
    person = addressbook_pb2.Person()
    person.id = 1234
    person.name = "John Doe"
    person.email = "jdoe@example.com"
    phone = person.phones.add()
    phone.number = "555-4321"
    phone.type = addressbook_pb2.Person.HOME

    return person

def test_exceptions(person):
    # non existing field
    with pytest.raises(AttributeError) as e:
        person.no_such_field = 1

    # invalid type
    with pytest.raises(TypeError) as e:
      person.id = '123'

def test02():
    assert addressbook_pb2.Person.WORK == 2

def test_message_interface(person):
    print(person)
    assert person.IsInitialized() == True
    assert str(person.email) == 'jdoe@example.com'

    # CopyFrom()
    # Clear()

def test_serialization(person):
    expected = b'\n\x08John Doe\x10\xd2\t\x1a\x10jdoe@example.com"\x0c\n\x08555-4321\x10\x01'

    assert person.SerializeToString() == expected

    # with pytest.raises(EncodeError) as e:
    #     addressbook_pb2.Person().SerializeToString()

    person_serialised = person.SerializeToString()
    new_person = addressbook_pb2.Person()
    new_person.ParseFromString(person_serialised)

    assert person == new_person
