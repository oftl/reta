from src.config_functional import ConfigFunctional as Config

def test_01():
    cfg = Config(configfile='./test_functional.yaml')

    assert str(cfg) == 'ConfigFunctional: one: 1, two: 2, three: 3'

    assert cfg.one == 1
    assert cfg.two == 2
    assert cfg.three == 3

def test_02():
    cfg = Config(configfile='./test_02.yaml')

    assert str(cfg) == 'ConfigFunctional: thing_a: 4.1, thing_b: 4.2, thing_c: 4.3'

    assert cfg.thing_a == 4.1
    assert cfg.thing_c == 4.3
    assert cfg.thing_b == 4.2
    print(cfg)

def test_03():
    cfg = Config(configfile='./test_03.yaml')

    assert cfg.a_list_0 == 'a'
    assert cfg.a_list_1 == 'b'
    assert cfg.a_list == ['a', 'b']
    assert cfg.a_dict_x == 'x'
    assert cfg.a_dict_y == 'y'

def test_04():
    cfg = Config(configfile='./test_04.yaml')

    assert cfg.LIST_0_martin_job == 'Developer'
    assert cfg.LIST_1_tabitha_skills_2 == 'erlang'

def test_05():
    cfg = Config(configfile='./test_05.yaml')

    assert cfg.devs_martin_skills_1 == 'perl'
    assert cfg.devs_tabitha_name == 'Tabitha Bitumen'
