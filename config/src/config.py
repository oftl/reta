import yaml
import re
import warnings
import abc

DEBUG = False

class Config(abc.ABC):
    def __init__(self, configfile=''):
        # NOTE
        # remember what attributes came from the configfile
        # used for __str__ only, access from outside must know these of course
        self.properties = []

        with open(configfile, 'r') as config:
            self.make_properties(cfg=yaml.load(config))

    def make_properties(self, cfg, prefix='', indent=0):
        DEBUG and print(f'{" " * indent}prefix: {prefix}')

        if isinstance(cfg, list):
            DEBUG and print(f'{" " * indent}isa list')

            for i, elem in enumerate(cfg):
                DEBUG and print(f'{" " * indent}{i}: {elem}')
                new_prefix = prefix + str(i) + '_'
                self.make_properties(cfg=elem, prefix=new_prefix, indent = indent + 2)

            self.new_property(prefix.rstrip('_'), cfg)


        elif isinstance(cfg, dict):
            DEBUG and print(f'{" " * indent}isa dict: {cfg}')

            for key, val in cfg.items():
                new_prefix = prefix + key + '_'
                DEBUG and print (f'{" " * indent}new_prefix: {new_prefix}, for val: {val}')
                self.make_properties(cfg=val, prefix=new_prefix, indent = indent + 2)

        else:
            DEBUG and print(f'{" " * indent}isa scalar: {prefix} -> {cfg}')

            self.new_property(key=prefix.rstrip('_'), val=cfg)

    def new_property(self, key, val):
        if re.match('^\d.*', key):
            warnings.warn('top level lists are not supported, prefixing with "LIST_"')
            key = 'LIST_' + key

        DEBUG and print(f'np key:{key}, val: {val}')
        self.properties.append(key)

        prop = self.create_property(key)

        setattr(self.__class__, key, prop)
        setattr(self, key, val)

    def __str__(self):
        out = []

        for key in self.properties:
            val = self.__dict__.get(key)
            out.append(f'{key}: {val}')

        return f'{self.__class__.__name__}: {", ".join(out)}'

    @abc.abstractmethod
    def create_property(self, name):
        pass
