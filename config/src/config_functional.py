from src.config import Config, DEBUG

class ConfigFunctional(Config):

    def create_property(self, name):
        # get/set values directly in __dict__ to bypass the property

        def getter(instance):
            DEBUG and print(f'getter of {name} instance id {id(instance)}')
            return instance.__dict__[name]

        def setter(instance, value):
            DEBUG and print(f'setter of {name} instance id {id(instance)}')
            instance.__dict__[name] = value

        return property(getter, setter)
