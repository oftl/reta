from src.config import Config, DEBUG

class Property:
    def __init__(self, storage_name):
        self.storage_name = storage_name

    def __set__(self, instance, value):
        DEBUG and print(f'__set__ of {self.storage_name} instance id {id(instance)}')
        instance.__dict__[self.storage_name] = value

    def __get__(self, instance, owner):
        DEBUG and print(f'__get__ of {self.storage_name} instance id {id(instance)}')
        return instance.__dict__[self.storage_name]

class ConfigDescriptor(Config):
    def create_property(self, name):
        return Property(name)
