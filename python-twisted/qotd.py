#!/usr/bin/env python2

from twisted.internet import reactor
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.internet.protocol import Factory, Protocol


class QOTD(Protocol):
    def connectionMade(self):
        # self.factory was set by the factory's default buildProtocol:
        self.transport.write(self.factory.quote + "\r\n")
        self.transport.loseConnection()


class QOTDFactory(Factory):
    protocol = QOTD

    def __init__(self, quote=None):
        self.quote = quote or "An apple a day keeps the doctor away"

    def startFactory(self):
        print("startFactory()")

    def stopFactory(self):
        print("stopFactory()")


# 8007 is the port you want to run under. Choose something >1024
endpoint = TCP4ServerEndpoint(reactor, 8007)
endpoint.listen(QOTDFactory())
reactor.run()
