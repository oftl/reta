import pytest
import time


# ### data descriptor

class DataDescriptor(object):
    """
    create storage keys from an incremented class variable __counter
    storage key is used as key in the instances __dict__
    instance is the managing (using) object - which is not to be confused
    with self in DataDescriptor it self
    """

    __counter: int = 0

    def __init__(self):
        cls = self.__class__
        cls.__counter += 1
        self.__storage_key = f'value#{self.__counter}'

    def __get__(self, instance, type):
        return instance.__dict__.get(self.__storage_key)

    def __set__(self, instance, value):
        instance.__dict__[self.__storage_key] = value


class ThingDD(object):
    dd = DataDescriptor()


def test_dd():
    a_thing = ThingDD()
    another_thing = ThingDD()

    a_thing.dd = 'A Thing'
    another_thing.dd = 'Another Thing'

    assert a_thing.dd == 'A Thing'
    assert another_thing.dd == 'Another Thing'


class ThingDDMulti(object):
    dd_1 = DataDescriptor()
    dd_2 = DataDescriptor()


def test_dd_02():
    thing = ThingDDMulti()
    thing.dd_1 = 'the first one'
    thing.dd_2 = 'the second one'

    assert thing.dd_1 == 'the first one'
    assert thing.dd_2 == 'the second one'


# ### read only data descriptor

class ReadOnlyDataDescriptor(object):
    def __get__(self, instance, type):
        return 42

    def __set__(self, instance, type):
        raise AttributeError


class ThingRODD(object):
    rodd = ReadOnlyDataDescriptor()


def test_rodd():
    thing = ThingRODD()

    with pytest.raises(AttributeError):
        thing.rodd = 23

    assert str(type(thing.rodd)) == "<class 'int'>"


# ### non data descriptor

class NonDataDescriptor(object):
    def __get__(self, instance, type):
        return lambda: time.time()


class ThingNDD(object):
    ndd = NonDataDescriptor()


def test_ndd():
    thing = ThingNDD()

    assert str(type(thing.ndd)) == "<class 'function'>"
    assert str(type(thing.ndd())) == "<class 'float'>"
