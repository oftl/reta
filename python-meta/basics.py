def run_codes(codes):
    space = ' '
    max_width = 50
    col_width = max([len(c) for c in codes])
    if col_width >= max_width:
        col_width = 30

    for c in codes:
        if c.startswith('#'):
            print(c)
        elif c:
            padding = f'{" " * (col_width - len(c))}'
            print(f'{c} {padding} {eval(c)}')
        else:
            print()

#####

class BaseClass:
    class_attr = 'BaseClass class_attr'

    def __init__(self):
        self.instance_attr = 'i am the BaseClass'

class DerivedClass(BaseClass):
    class_attr = 'DerivedClass class_attr'

    def __init__(self):
        self.instance_attr = 'i am the DerivedClass'

class LockedClass():
    __slots__ = ('instance_attr',)
    class_attr = 'DerivedClass class_attr'

    def __init__(self):
        self.instance_attr = 'i am the DerivedClass'

base_class = BaseClass()
derived_class = DerivedClass()
locked_class = LockedClass()


run_codes([
    'BaseClass.class_attr',
    'base_class.class_attr',
    'base_class.instance_attr',
    '',
    'DerivedClass.class_attr',
    'derived_class.class_attr',
    'derived_class.instance_attr',
    '',
    'DerivedClass.__class__',
    'DerivedClass.__class__',
    '',
    'hasattr(DerivedClass, "class_attr")',
    'getattr(DerivedClass, "class_attr")',
    'hasattr(DerivedClass, "instance_attr")',
    'hasattr(derived_class, "instance_attr")',
    'getattr(derived_class, "instance_attr")',
    '',
    'base_class.__class__',
    '# base_class.__slots__',
    'base_class.__dict__',
    'vars(base_class),'
    '',
    'locked_class.__class__',
    'locked_class.__slots__',
    '# locked_class.__dict__',
    '# vars(locked_class)',
    'locked_class.instance_attr',
    '',
    '# dot-notation and builtins (get|set|has)attr) alike trigger the following',
    '# del',
    '#   __delattr__(self, name)',
    '# dir()',
    '#   __dir__(self)',
    '# retrieve attribute, except special attributes',
    '#   __getattribute__(self, name)',
    '# retrieve attribute, invoked when __getattribute__ raises AttributeErro, rafter search in object, class and superclass fail',
    '#   __getattr__(self, name)',
    '# => __getattribute__, obj/class/superclass, __getattr_ ?!',
    '# set attribute',
    '# __setattribute__(self, name, value)',
])
